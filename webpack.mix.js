const mx = require("laravel-mix");
const fs = require("fs-extra");
// require("laravel-mix-bundle-analyzer");

mx.setPublicPath("public/assets");
// mx.bundleAnalyzer();

const mapList = [
  "public/assets/js/manifest.js.map",
  "public/assets/js/vendor.js.map",
  "public/assets/js/app.js.map",
  "public/assets/css/app.css.map"
];

if (mx.inProduction()) {
  mapList.forEach(map =>
    fs.unlink(map, e => console.log("Can't Find: ", e.path))
  );
} else {
  mx.webpackConfig({ devtool: "source-map" }).sourceMaps();
}

mx
  .options({ processCssUrls: false })
  .react("src/react/index.js", "public/assets/js/app.js")
  .extract(["react", "react-dom"])
  .sass("src/sass/app.scss", "public/assets/css/app.css");
