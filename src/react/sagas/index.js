import { takeEvery, put, call } from "@redux-saga/core/effects";
import { getUsers } from "../api";
import { createIndexByUniqueProp } from "../libraries/Helpers";

export function* watcherSaga() {
  yield call(getUsersWorker);
  yield takeEvery("GET_USERS", getUsersWorker);
}

export function* getUsersWorker() {
  const response = yield getUsers();
  yield put({
    type: "UPDATE",
    data: {
      list: response.data.records,
      index: createIndexByUniqueProp(response.data.records, "id")
    }
  });
}
