import ReactDOM from "react-dom";
import React from "react";
import App from "./App";
import { store } from "./store";
import { Provider } from "react-redux";
import { sagaMiddleware } from "./middleware";
import { watcherSaga } from "./sagas";
// import * as serviceWorker from './serviceWorker';

sagaMiddleware.run(watcherSaga);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
// serviceWorker.unregister();
