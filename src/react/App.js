import React, { Component, Fragment } from "react";
import { HashRouter as Router, Route } from "react-router-dom";
import UserForm from "./components/Forms/UserForm";
import TableViewer from "./components/TableViewer";
import "./libraries/FontAwesome";
import NavBar from "./components/Navs/NavBar";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Fragment>
          <NavBar />
          <div className="container-fluid main">
            <div className="container">
              <Route exact path="/" component={() => <UserForm verbose />} />
              <Route exact path="/table" component={() => <TableViewer />} />
            </div>
          </div>
        </Fragment>
      </Router>
    );
  }
}
