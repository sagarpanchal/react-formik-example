/**
 * Makes immutable copy of an object
 * @param {object} obj an object
 */
export const cloneObject = obj => {
  let copy;

  // Handle the 3 simple types, and null or undefined
  if (null == obj || "object" != typeof obj) {
    return obj;
  }

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = cloneObject(obj[i]);
    }
    return copy;
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {};
    for (let attr in obj) {
      obj.hasOwnProperty(attr) ? (copy[attr] = cloneObject(obj[attr])) : null;
    }
    return copy;
  }

  throw new Error("Unable to copy obj! Its type isn't supported.");
};

/**
 * Converts pascal/camel case to sentence case
 * @param {string} string string in pascal or lowercase
 */
export const formatLabel = string => {
  let output = string.replace(/(?:^|\.?)([A-Z])/g, x => " " + x.toLowerCase());
  return output.charAt(0).toUpperCase() + output.slice(1);
};

/**
 * Returns index of the object by specified property
 * @param {object} array
 * @param {string} prop property
 */
export const createIndexByProp = (object = [], prop) => {
  object = cloneObject(object);
  let returnObject = {};
  object.forEach((item, i) => {
    !returnObject.hasOwnProperty(item[prop])
      ? (returnObject[item[prop]] = [])
      : null;
    returnObject[item[prop]].push(i);
  });
  return returnObject;
};

/**
 * Returns index of the object by specified property
 * overwrites dplicate values
 * @param {object} object
 * @param {string} prop property
 */
export const createIndexByUniqueProp = (object = [], prop) => {
  object = cloneObject(object);
  let returnObject = {};
  object.forEach((item, i) => {
    returnObject[item[prop]] = i;
  });
  return returnObject;
};

/**
 * Returns a copy of the object indexed by specified property
 * @param {object} object
 * @param {string} prop property
 * @param {boolean} delProp weather to delete the prop that is being used for indexing
 */
export const withIndex = (array = [], prop, delProp = false) => {
  array = cloneObject(array);
  let returnObject = {};
  array.forEach(item => {
    !returnObject.hasOwnProperty(item[prop]) && (returnObject[item[prop]] = []);
    returnObject[item[prop]].push(item);
    let pos = returnObject[item[prop]].length - 1;
    delProp && delete returnObject[item[prop]][pos][prop];
  });
  return returnObject;
};
