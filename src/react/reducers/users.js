const initState = {
  list: [],
  index: {}
};

const users = (state = initState, action) => {
  switch (action.type) {
    case "UPDATE":
      return action.data;
    default:
      return state;
  }
};

export default users;
