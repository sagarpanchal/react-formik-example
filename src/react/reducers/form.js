const initState = {
  error: "",
  success: false
};

const form = (state = initState, action) => {
  switch (action.type) {
    case "REG_FAILURE":
      return { error: action.data, success: false };

    case "REG_SUCCESS":
      return { error: "", success: true };

    case "REG_RESET":
      return { ...initState };

    default:
      return state;
  }
};

export default form;
