import Axios from "axios";

const axios = Axios.create();
axios.defaults.timeout = 5000;

const url = "/api.php/records/users";

export const addUser = async data => {
  try {
    const response = await axios.post(url, data);
    return await response;
  } catch (e) {
    return e.response;
  }
};

export const getUsers = () => {
  try {
    return axios.get(url);
  } catch (e) {
    return e.response;
  }
};

export const matchEmail = async email => {
  try {
    const response = await axios.get(
      url + "?include=id&filter=email,eq," + email
    );
    return response.data.records.length;
  } catch (e) {
    return 0;
  }
};
