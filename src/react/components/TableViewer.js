import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
// import cellEditFactory from "react-bootstrap-table2-editor";

import { connect } from "react-redux";
import Loader from "./Loader";

class TableViewer extends Component {
  static propTypes = {
    users: PropTypes.array
    // dispatch: PropTypes.func
  };

  componentDidMount() {
    Loader.hide();
  }

  render() {
    const { users } = this.props;

    const columns = [
      { dataField: "id", text: "User ID", sort: true },
      { dataField: "firstName", text: "First Name", sort: true },
      { dataField: "email", text: "Email", sort: true }
    ];

    return (
      <Fragment>
        <Loader />
        <div className="d-flex flex-row justify-content-center">
          <div className="col-sm-12 col-md-9 col-lg-7 p-0 mb-3 p-0">
            <BootstrapTable
              bootstrap4
              hover
              bordered={false}
              columns={columns}
              data={users}
              headerClasses="thead-dark"
              keyField="id"
              noDataIndication="Table is Empty"
              pagination={paginationFactory()}
              selectRow={{ mode: "checkbox" }}
            />
            {/* cellEdit={cellEditFactory({ mode: "click" })} */}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(
  state => {
    return {
      users: state.users.list
    };
  },
  dispatch => {
    return {
      dispatch: obj => dispatch(obj)
    };
  }
)(TableViewer);
