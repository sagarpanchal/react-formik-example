import React, { Component } from "react";

import Collapse from "reactstrap/es/Collapse";
import Nav from "reactstrap/es/Nav";
import Navbar from "reactstrap/es/Navbar";
import NavbarBrand from "reactstrap/es/NavbarBrand";
import NavbarToggler from "reactstrap/es/NavbarToggler";
import NavItem from "reactstrap/es/NavItem";
import NavLink from "reactstrap/es/NavLink";

import Link from "react-router-dom/es/NavLink";

class NavBar extends Component {
  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  render() {
    return (
      <Navbar color="dark" dark expand="md" fixed="top">
        <NavbarBrand href="#">React App</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={Link} exact to="/">
                Form
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/table">
                Table
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

export default NavBar;
