import React, { Component } from "react";
import PropTypes from "prop-types";
import Prism from "prismjs";
import ReactHTMLPraser from "react-html-parser";
import unescape from "lodash/unescape";

class FormikState extends Component {
  static propTypes = {
    state: PropTypes.object,
    classes: PropTypes.string
  };

  render() {
    let { state, classes, ...otherProps } = this.props;
    let code = Prism.highlight(
      unescape(JSON.stringify(state, null, 2)),
      Prism.languages.javascript
    ).replace(/\n/g, "</br>");

    !classes && (classes = "");
    classes += " language-json";
    return (
      <pre className={classes} {...otherProps}>
        {ReactHTMLPraser(code)}
      </pre>
    );
  }
}

export default FormikState;
