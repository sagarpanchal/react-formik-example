import regEx from "../../../libraries/RegEx";
import { matchEmail } from "../../../api";
import { formatLabel } from "../../../libraries/Helpers";

export const validateFields = (values, regExp, customError) => {
  let fields = Object.keys(values);
  let errors = {};
  fields.forEach(field => {
    !values[field] && (errors[field] = formatLabel(field) + " is required");
    if (!errors[field] && regExp[field]) {
      if (!regExp[field].test(values[field])) {
        errors[field] = customError.hasOwnProperty(field)
          ? customError[field]
          : "Invalid value";
      }
    }
  });
  return errors;
};

export const validateUserForm = values => {
  const { firstName, lastName, phone, birthdate, color, notes } = values;
  return validateFields(
    { firstName, lastName, phone, birthdate, color, notes },
    { firstName: regEx.name, lastName: regEx.name, phone: regEx.phone },
    { phone: "Must be =10 numbers" }
  );
};

export const checkEmail = async value => {
  let response = await matchEmail(value);
  return response ? "This email is taken" : null;
};

export const validateEmail = async value => {
  let error = validateFields(
    { email: value },
    { email: regEx.email },
    { email: "Invalid address" }
  ).email;

  return error ? error : checkEmail(value);
};
