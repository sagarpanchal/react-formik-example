import React, { Component } from "react";
import PropTypes from "prop-types";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Field } from "formik";

import FormGroup from "reactstrap/es/FormGroup";
import Label from "reactstrap/es/Label";

import { formatLabel } from "../../../libraries/Helpers";

export class RenderFieldLabel extends Component {
  static propTypes = {
    htmlFor: PropTypes.string,
    text: PropTypes.string
  };

  focusInput = ({ target }) => {
    let field = document.getElementsByName(target.htmlFor)[0];
    field !== undefined && field.focus();
  };

  render() {
    const { htmlFor, text, ...rest } = this.props;
    return (
      <Label htmlFor={htmlFor} onClick={this.focusInput} {...rest}>
        {text}
      </Label>
    );
  }
}

export class RenderFormError extends Component {
  static propTypes = {
    touched: PropTypes.bool,
    error: PropTypes.string
  };

  render() {
    const { touched, error } = this.props;
    return touched && error ? (
      <div className="feedback">
        <small className="text-danger">
          <FontAwesomeIcon icon="exclamation-circle" className="mr-1" />
          {error}
        </small>
      </div>
    ) : null;
  }
}

/**
 * render field
 */
export class RenderField extends Component {
  static propTypes = {
    fieldName: PropTypes.string.isRequired,
    fieldTitle: PropTypes.string,
    type: PropTypes.string,
    formikProps: PropTypes.object.isRequired,
    required: PropTypes.bool,
    classes: PropTypes.string
  };

  render() {
    let {
      fieldName,
      fieldTitle,
      type,
      formikProps,
      classes,
      ...otherProps
    } = this.props;
    let { handleBlur, handleChange, values, touched, errors } = formikProps;
    !fieldTitle && (fieldTitle = formatLabel(fieldName));
    !type && (type = "text");
    !classes && (classes = "form-control");
    touched[fieldName] && errors[fieldName] && (classes += " is-invalid");

    return (
      <FormGroup className="has-float-label">
        <Field
          className={classes}
          type={type}
          name={fieldName}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values[fieldName]}
          placeholder=""
          {...otherProps}
        />
        <RenderFieldLabel htmlFor={fieldName} text={fieldTitle} />
        <RenderFormError
          touched={touched[fieldName]}
          error={errors[fieldName]}
        />
      </FormGroup>
    );
  }
}

export class RenderTextArea extends Component {
  static propTypes = {
    fieldName: PropTypes.string.isRequired,
    fieldTitle: PropTypes.string,
    formikProps: PropTypes.object.isRequired,
    required: PropTypes.bool,
    classes: PropTypes.string
  };

  render() {
    let {
      fieldName,
      fieldTitle,
      formikProps,
      classes,
      ...otherProps
    } = this.props;
    let { handleBlur, handleChange, values, touched, errors } = formikProps;
    !fieldTitle && (fieldTitle = formatLabel(fieldName));
    !classes && (classes = "form-control");
    touched[fieldName] && errors[fieldName] && (classes += " is-invalid");

    return (
      <FormGroup className="has-float-label">
        <Field
          className={classes}
          component="textarea"
          name={fieldName}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values[fieldName]}
          placeholder=""
          {...otherProps}
        />
        <RenderFieldLabel htmlFor={fieldName} text={fieldTitle} />
        <RenderFormError
          touched={touched[fieldName]}
          error={errors[fieldName]}
        />
      </FormGroup>
    );
  }
}

export class RenderSelect extends Component {
  static propTypes = {
    fieldName: PropTypes.string.isRequired,
    fieldTitle: PropTypes.string,
    options: PropTypes.array,
    formikProps: PropTypes.object.isRequired,
    required: PropTypes.bool,
    classes: PropTypes.string
  };

  render() {
    let {
      fieldName,
      fieldTitle,
      options,
      formikProps,
      classes,
      ...otherProps
    } = this.props;
    let { handleBlur, handleChange, values, touched, errors } = formikProps;
    !fieldTitle && (fieldTitle = formatLabel(fieldName));
    !classes && (classes = "form-control custom-select");
    touched[fieldName] && errors[fieldName] && (classes += " is-invalid");

    return (
      <FormGroup className="has-float-label">
        <Field
          component="select"
          className={classes}
          name={fieldName}
          onChange={handleChange}
          onBlur={handleBlur}
          value={values[fieldName]}
          {...otherProps}
        >
          {options.map((option, i) => (
            <option key={i} value={option.value}>
              {option.text}
            </option>
          ))}
        </Field>
        <RenderFieldLabel htmlFor={fieldName} text={fieldTitle} />
        <RenderFormError
          touched={touched[fieldName]}
          error={errors[fieldName]}
        />
      </FormGroup>
    );
  }
}

export class RenderSwitch extends Component {
  static propTypes = {
    fieldName: PropTypes.string.isRequired,
    fieldTitle: PropTypes.string,
    formikProps: PropTypes.object.isRequired,
    required: PropTypes.bool,
    classes: PropTypes.string
  };

  render() {
    let { fieldName, fieldTitle, formikProps, ...otherProps } = this.props;
    let { handleBlur, handleChange, values, touched, errors } = formikProps;
    !fieldTitle && (fieldTitle = formatLabel(fieldName));

    return (
      <FormGroup className="custom-control custom-switch">
        <Field
          component="input"
          type="checkbox"
          className="custom-control-input"
          name={fieldName}
          id={fieldName}
          onChange={handleChange}
          onBlur={handleBlur}
          checked={values[fieldName]}
          {...otherProps}
        />
        <RenderFieldLabel
          htmlFor={fieldName}
          text={fieldTitle}
          className="custom-control-label"
        />
        <RenderFormError
          touched={touched[fieldName]}
          error={errors[fieldName]}
        />
      </FormGroup>
    );
  }
}
