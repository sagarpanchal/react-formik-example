import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import { Form, Formik } from "formik";
import { connect } from "react-redux";

import Button from "reactstrap/es/Button";
import Card from "reactstrap/es/Card";
import CardBody from "reactstrap/es/CardBody";
import CardHeader from "reactstrap/es/CardHeader";

import ReactNotification from "react-notifications-component";

import {
  RenderField,
  RenderSelect,
  RenderSwitch,
  RenderTextArea
} from "./components/Fields";
import { validateUserForm, validateEmail } from "./components/Validate";
import { addUser } from "../../api";
import FormikState from "./components/FormikState";
import Loader from "../Loader";

class UserForm extends Component {
  static propTypes = {
    formError: PropTypes.string,
    submitSuccess: PropTypes.bool,
    dispatch: PropTypes.func,
    verbose: PropTypes.bool
  };

  notification = "";
  notificationDOMRef = React.createRef();

  addNotification = () => {
    const { formError, submitSuccess } = this.props;
    const commonProps = {
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: { duration: 10000 },
      dismissable: { click: true }
    };
    this.notificationDOMRef.current.removeNotification(this.notification);
    submitSuccess &&
      !formError &&
      (this.notification = this.notificationDOMRef.current.addNotification({
        title: "User Registered",
        message: "Entry added",
        type: "success",
        ...commonProps
      }));
    !submitSuccess &&
      formError &&
      (this.notification = this.notificationDOMRef.current.addNotification({
        title: "Error",
        message: formError,
        type: "danger",
        ...commonProps
      }));
  };

  componentDidMount() {
    Loader.hide();
  }

  componentDidUpdate() {
    this.addNotification();
  }

  handleSubmit = async (values, formikBag) => {
    const { setSubmitting, resetForm } = formikBag;
    const { dispatch } = this.props;
    setSubmitting(true);
    addUser(values).then(response => {
      const { status, data } = response;
      dispatch({ type: "REG_RESET" });
      if (status === 200 && data > 0) {
        resetForm();
        dispatch({ type: "REG_SUCCESS" });
        dispatch({ type: "GET_USERS" });
      } else if (
        data.hasOwnProperty("code") &&
        data.hasOwnProperty("message")
      ) {
        dispatch({
          type: "REG_FAILURE",
          data: data.code + ": " + data.message
        });
      }
      setSubmitting(false);
    });
  };

  renderForm = props => {
    const {
      dirty,
      errors,
      handleBlur,
      handleChange,
      isSubmitting,
      isValid,
      touched,
      values
    } = props;

    const formikProps = {
      handleBlur,
      handleChange,
      values,
      touched,
      errors
    };

    const { verbose } = this.props;

    const selectOptions = [
      { value: "", text: "-- Select --" },
      { value: "red", text: "Red" },
      { value: "green", text: "Green" },
      { value: "blue", text: "Blue" }
    ];

    return (
      <Fragment>
        <div className="d-flex flex-row justify-content-center">
          <Card className="col-sm-12 col-md-9 col-lg-6 p-0 mb-3">
            <CardHeader>Form</CardHeader>
            <CardBody>
              <div className={"submitting" + (isSubmitting ? "" : " d-none")}>
                <div className="spinner-parent">
                  <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>
              </div>
              <Form className="w-100 float-left pt-2">
                <div className="row m-0">
                  <div className="col-12 col-sm-6 p-0 col-sm-right">
                    <RenderField
                      fieldName="firstName"
                      formikProps={formikProps}
                    />
                  </div>
                  <div className="col-12 col-sm-6 p-0 col-sm-left">
                    <RenderField
                      fieldName="lastName"
                      formikProps={formikProps}
                    />
                  </div>
                </div>
                <div className="row m-0">
                  <div className="col-12 col-sm-6 p-0 col-sm-right">
                    <RenderField
                      fieldName="email"
                      type="email"
                      formikProps={formikProps}
                      validate={validateEmail}
                    />
                  </div>
                  <div className="col-12 col-sm-6 p-0 col-sm-left">
                    <RenderField fieldName="phone" formikProps={formikProps} />
                  </div>
                </div>
                <div className="row m-0">
                  <div className="col-12 col-sm-6 p-0 col-sm-right">
                    <RenderField
                      fieldName="birthdate"
                      type="date"
                      formikProps={formikProps}
                    />
                  </div>
                  <div className="col-12 col-sm-6 p-0 col-sm-left">
                    <RenderSelect
                      fieldName="color"
                      options={selectOptions}
                      formikProps={formikProps}
                    />
                  </div>
                </div>
                <RenderSwitch fieldName="employed" formikProps={formikProps} />
                <RenderTextArea fieldName="notes" formikProps={formikProps} />
                <Button
                  size="sm"
                  className="float-right"
                  type="submit"
                  disabled={!dirty || !isValid || isSubmitting}
                >
                  Submit
                </Button>
                <Button
                  size="sm"
                  outline
                  className="float-left mr-2"
                  type="reset"
                  disabled={!dirty || isSubmitting}
                >
                  Clear
                </Button>
              </Form>
            </CardBody>
          </Card>
        </div>
        {verbose && (
          <div className="d-flex flex-row justify-content-center">
            <FormikState
              state={{ ...props }}
              classes="col-sm-12 col-md-9 col-lg-6 mt-0 mb-3"
            />
          </div>
        )}
      </Fragment>
    );
  };

  render() {
    const initialValues = {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      birthdate: "",
      color: "",
      employed: false,
      notes: ""
    };

    return (
      <Fragment>
        <Loader />
        <ReactNotification ref={this.notificationDOMRef} />
        <Formik
          initialValues={initialValues}
          isInitialValid={false}
          validate={validateUserForm}
          onSubmit={this.handleSubmit}
          render={this.renderForm}
        />
      </Fragment>
    );
  }
}

export default connect(
  state => {
    return {
      formError: state.form.error,
      submitSuccess: state.form.success
    };
  },
  dispatch => {
    return {
      dispatch: obj => dispatch(obj)
    };
  }
)(UserForm);
