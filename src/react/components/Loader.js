import React, { Component } from "react";
import Velocity from "velocity-animate";

class Loader extends Component {
  static hide() {
    setTimeout(() => {
      Velocity(document.getElementsByClassName("loader")[0], "fadeOut", {
        duration: 320
      });
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    }, 120);
  }

  render() {
    return (
      <div
        className="loader"
        style={{
          background: "#fff",
          height: "100%",
          width: "100%",
          position: "fixed",
          top: 0,
          left: 0,
          zIndex: 5
        }}
      />
    );
  }
}

export default Loader;
